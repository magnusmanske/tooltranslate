var mode = '' ;
var url_params = {} ;
var languages = {} ;
var current_tool = '' ;
var current_languages = ['en'] ;
var toolinfo = {} ;
var tools = {} ;
var tt = {} ; // ToolTranslate interface
var tt2 = {} ; // Translations for tool to edit
var auth = { logged_in:0 } ;
var just_missing = false ;
var tooltrans_languages = [] ;
var currently_editing = {} ;
var warn_english = true ;

function isLoggedIn () {
	return auth.logged_in ;
}

function getUserName () {
	if ( !isLoggedIn() ) return "" ;
	return auth.result.query.userinfo.name ;
}

function getHashVars () {
	var vars = {} ;
	var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
	$.each ( hashes , function ( i , j ) {
		var hash = j.split('=');
		hash[1] += '' ;
		vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
	} ) ;
	return vars;
}


function updateHash () {
	var h = [] ;
	if ( current_tool != '' ) h.push ( 'tool='+current_tool ) ;
	if ( current_languages.length != 1 || current_languages[0] != 'all' ) h.push ( 'languages='+current_languages.join(',') ) ;
	if ( just_missing ) h.push ( 'just_missing=1' ) ;
	if ( tt.language != 'en' ) h.push ( 'interface_language='+tt.language ) ;
	if ( mode != '' ) h.push ( 'mode='+mode ) ;
	h = h.join ( '&' ) ;
	window.location.hash = h ;
}

function escapeHTML ( s ) {
	return $('<textarea />').html(s).text() ;
}

function cancelEditing () {
	currently_editing = {} ;
	$('td.translation_editing').each ( function () {
		var o = $(this) ;
		o.find('div.editing').html('').hide() ;
		o.find('div.existing_text').show() ;
		o.removeClass ( 'translation_editing' ) ;
	} ) ;
}

function storeTranslation ( o ) {
	if ( tt2.t(o.key,{lang:o.lang}) == o.text ) return ; // No need
	
	if ( o.text == '' ) {
		var res = window.confirm ( tt.t('blank_warning') ) ;
		if ( !res ) return ;
	}
	
	if ( current_tool == 1 ) { // Updating own interface
		tt.setCache ( o.lang , o.key , o.text ) ;
		if ( tt.language == o.lang ) tt.updateInterface() ;
	}
	
	tt2.setCache ( o.lang , o.key , o.text ) ;
	var p = o.td.find('div.existing_text').parent() ;
	p.addClass ( 'label-warning' ) ;
	p.find('div.existing_text').text ( escapeHTML ( o.text ) ) ;
	$.get ( 'api.php' , {
		mode:'store_translation',
		language:o.lang,
		key:o.key,
		json:JSON.stringify(o.text),
		tool:current_tool
	} , function ( d ) {
		p.removeClass ( 'label-warning' ) ;
		
		if ( d.data != 'OK' ) {
			p.addClass ( 'label-danger' ) ;
			console.log ( d.data ) ;
			return ;
		}

		p.addClass ( 'label-success' ) ;
		setTimeout ( function () {
			p.removeClass ( 'label-success' ) ;
		} , 2000 ) ;
	} , 'json' ) ;
}

function startEditing ( o ) {
	var key = o.attr ( 'key' ) ;
	var lang = o.attr ( 'lang' ) ;
	
	if ( lang == '' || lang == 'undefined' || typeof lang == 'undefined' ) {
		alert ( "What, no language? Can't edit that!" ) ;
		return ;
	}
	
	if ( lang == 'en' && warn_english ) {
		var res = window.confirm ( tt.t('warn_english') ) ;
		if ( !res ) return ;
		warn_english = false ;
	}

	if ( currently_editing.key == key && currently_editing.lang == lang ) return false ; // Double-click in current edit cell
	cancelEditing() ; // Close all other edits
	currently_editing = { key:key , lang:lang } ;
	o.addClass ( 'translation_editing' ) ;
	o.find('div.existing_text').hide() ;
	
	var h = "" ;
	h += "<form class='form'>" ;
	h += "<div style='width:400px'>" ;
	h += "<textarea tt_placeholder='textarea_placeholder' class='form-control edit' style='margin-bottom:10px;width:100%' rows=1></textarea>" ;
	h += "<div style='font-size:xx-small' tt='license'></div>" ;
	h += "<div style='width:100%;text-align:right'>" ;
	h += "<button type='button' class='btn btn-primary-outline ok' tt='button_ok'></button> &nbsp; " ;
	h += "<button type='button' class='btn btn-danger-outline cancel' tt='button_cancel'></button>" ;
	h += "</div>" ;
	h += "</div>" ;
	h += "<div class='suggestions'></div>" ;
	h += "</form>" ;
	
	o.find('div.editing').html(h).show() ;
	tt.updateInterface ( o.find('div.editing') ) ;
	o.find('div.editing div[tt="license"] a').attr ( { tabindex:-1 } ) ;
	
	o.find('button.ok').click ( function () {
		var text = o.find('textarea.edit').val() ;
		cancelEditing() ;
		storeTranslation ( { text:text , td:o , key:key , lang:lang } ) ;
	} ) ;
	o.find('button.cancel').click ( function () {
		cancelEditing() ;
	} ) ;
	
	$.get ( 'api.php' , {
		mode:'suggest_translations',
		language:lang,
		key:key,
		skip_tool:current_tool
	} , function ( d ) {
		if ( typeof d.data == 'undefined' || d.data.length == 0 ) return ;
		var h = "<div tt='suggestion'></div><ul>" ;
		$.each ( d.data , function ( k , v ) {
			var t = escapeHTML ( v ) ;
			h += "<li><a href='#' class='suggestion'>" + t + "</a></li>" ;
		} ) ;
		h += "</ul>" ;
		o.find('div.suggestions').html ( h ) ;
		tt.updateInterface ( o.find('div.suggestions') ) ;
		o.find('div.suggestions a.suggestion').click ( function () {
			var x = $(this) ;
			o.find('textarea.edit').html ( x.html() ) ;
			return false ;
		} ) ;
	} , 'json' ) ;
	
	var content = tt2.t ( key , {lang:lang} ) ;
	if ( typeof content != 'undefined' ) o.find('div.editing textarea').html ( content ) ;
	o.find('div.editing textarea').focus() ;
}

function getTranslationRow ( key ) {
	var h = '' ;
	h += "<tr>" ;
	h += "<th>" + key + "</th>" ;
	$.each ( current_languages , function ( dummy1 , lang ) {
//		var note = isLoggedIn() ? 'doubleclick2edit' : 'login2edit' ; //  tt_title='"+note+"'
		var t = escapeHTML ( tt2.t ( key , { lang:lang } ) ) ;
		h += "<td class='translation' lang='"+escattr(lang)+"' key='"+escattr(key)+"'>" ;
		h += "<div class='editing'></div>" ;
		h += "<div class='existing_text'>" ;
		if ( typeof t == 'undefined' ) {
			h += "&mdash;" ;
		} else {
			h += t ;
		}
		h += "</div>" ;
		h += "</td>" ;
	} ) ;
	h += "</tr>" ;
	return h ;
}

function displayTranslations () {
	var h = "" ;
	h += "<h1>" ;
	if ( tools[current_tool].url != '' ) h += "<a href='" + tools[current_tool].url + "' target='_blank'>" ;
	h += tools[current_tool].label ;
	if ( tools[current_tool].url != '' ) h += "</a>" ;
	h += "</h1>" ;
	h += "<div>" ;
	h += tools[current_tool].name ;
	h += " | <a href='https://meta.wikimedia.org/wiki/Special:CentralAuth/"+encodeURIComponent(tools[current_tool].owner)+"' target='_blank'>"+tools[current_tool].owner+"</a>" ;
	h += " | <a href='#' id='refresh_tool_json' tt='refresh_tool_json'></a>" ;
	h += "</div>" ;
	h += "<div>" ;
	h += "<label><input type='checkbox' id='just_missing'" ;
	if ( just_missing ) h += " checked" ;
	h += "> <span tt='just_missing'></span></label>" ;
	h += "</div>" ;
	h += "<div style='border-bottom:1px solid #DDD;margin-bottom:10px;padding-bottom:2px'>" ;
	$.each ( toolinfo.languages , function ( k , v ) {
		var txt = languages[v] ;
		if ( typeof txt == 'undefined' ) txt = '('+v+')' ;
		h += "<div style='display:inline-block;margin-right:20px;'>" ;
		h += "<a href='#' class='toggle_language' lang='"+v+"'>" + txt + "</a>" ;
		h += "</div>" ;
	} ) ;
	$.each ( current_languages , function ( k , v ) {
		if ( -1 != $.inArray ( v , toolinfo.languages ) ) return ;
		h += "<div style='display:inline-block;margin-right:20px;'>" ;
		h += "<a href='#' class='toggle_language' lang='"+v+"'>" + languages[v] + "</a>" ;
		h += "</div>" ;
	} ) ;
	h += "<div style='display:inline-block'><a href='#' class='add_language' tt='add_symbol' tt_title='add_language'></a></div>" ;
	h += "</div>" ;

	h += "<div>" ;
	h += "<table id='translation_table' class='table table-condensed table-striped'>" ;
	h += "<thead><tr><th tt='key'></th>" ;
	$.each ( current_languages , function ( k , v ) {
		h += "<th>" + languages[v] + "</th>" ;
	} ) ;
	h += "</tr></thead><tbody>" ;
	
	var keys = tt2.getJoinedKeys() ;
	$.each ( keys , function ( dummy , key ) {
	
		if ( just_missing ) {
			var has_missing = false ;
			$.each ( current_languages , function ( dummy1 , lang ) {
				var t = tt2.t ( key , {lang:lang} ) ;
				if ( typeof t == 'undefined' ) has_missing = true ;
			} ) ;
			if ( !has_missing ) return ;
		}
		
		h += getTranslationRow ( key ) ;
	
	} ) ;
	
	h += "</tbody></table>" ;
	h += "</div>" ;
	
	if ( isLoggedIn() ) {
		h += "<div style='margin-bottom:100px'><button type='button' class='btn btn-primary-outline' id='add_key' tt='button_add_key'></button></div>" ;
	}

	$('#output').html ( h ) ;
	tt.updateInterface ( '#output' ) ;
	$('#refresh_tool_json').click ( function () {
		$.get ( 'api.php' , {
			mode:'update_json',
			toolname:tools[current_tool].name
		} , function ( d ) {
		} , 'json' ) ;
		return false ;
	} ) ;
	$('#just_missing').change ( function () {
		just_missing = this.checked ;
		showTranslations() ;
	} ) ;
	$('#add_key').click ( function () {
		var key = prompt ( tt.t('enter_key_name') ) ;
		if ( key == null ) return ;
		key = key.replace ( /[^a-z0-9_-]/g , '' ) ;
		if ( key == '' ) return ;
		if ( -1 != $.inArray ( key , keys ) ) {
			alert ( tt2.t('key_exists') ) ;
			return ;
		}
		cancelEditing() ; // Paranoia
		var h = $(getTranslationRow ( key ) );
		$('#translation_table tbody').append ( h ) ;
		h.find('td.translation').dblclick ( function () { startEditing ( $(this) ) ; } ) ;
	} ) ;
	if ( isLoggedIn() ) {
		$('#output td.translation').dblclick ( function () {
			startEditing ( $(this) ) ;
		} ) ;
	}
	$('#output a.add_language').click ( function () {
		var l = prompt ( tt.t('add_language_prompt') ) ;
		if ( l == null ) return ;
		l = l.replace ( /[^a-z_-]/g , '' ) ;
		if ( l == '' ) return ;
		if ( -1 != $.inArray ( l , current_languages ) ) return ;
		if ( -1 != $.inArray ( l , toolinfo.languages ) ) return ;
		if ( typeof languages[l] == 'undefined' ) {
			alert ( "Language code " + l + " not recognised" ) ;
			return ;
		}
		current_languages.push ( l ) ;
		toolinfo.languages.push ( l ) ;
		showTranslations() ;
		return false ;
	} ) ;
	$('#output a.toggle_language').click ( function () {
		var o = $(this) ;
		var l = o.attr('lang') ;
		if ( -1 != $.inArray ( l , current_languages ) ) {
			current_languages = $.grep(current_languages, function(value) { return value != l; });
		} else {
			current_languages.push ( l ) ;
		}
		showTranslations() ;
		return false ;
	} ) ;
}

function showTranslations () {
	updateHash() ;
	tt2 = new ToolTranslation ( {
		tool:tools[current_tool].name,
		force_fresh:true ,
		no_interface_update:true,
		debug:true,
		languages:current_languages, // toolinfo.languages
		callback:function(){displayTranslations()}
	} ) ;
}

function showTool ( id ) {
	if ( typeof tools[id] == 'undefined' ) return ;
	var t = tools[id] ;
	current_tool = id ;
//	current_languages = ['en'] ;
	updateHash() ;
	
	var url = '/data/' + tools[id].name + '/toolinfo.json' ;
	$.get ( url , function ( d ) {
		toolinfo = d ;
		showTranslations() ;
	} ) ;
/*	
	$.get ( 'api.php' , {
		mode:'get_tool_info',
		id:current_tool
	} , function ( d ) {
		toolinfo = d.data ;
		showTranslations() ;
	} , 'json' ) ;*/
}

function showTools () {
	var h = '' ;
	$.each ( tools , function ( k , v ) {
		h += '<a class="dropdown-item" href="#" tool_id="'+v.id+'">'+v.label+'</a>' ;
	} ) ;
	
	$('#tools').html ( h ) ;
	$('#tools a.dropdown-item').click ( function () {
		var o = $(this) ;
		showTool ( o.attr('tool_id') ) ;
		$('#tools').parent().find('button[aria-expanded=true]').click() ;
		return false ;
	} ) ;
}

function showInterfaceLanguages ( langs ) {
	var h = '' ;
	$.each ( langs , function ( k , v ) {
		h += '<a class="dropdown-item" href="#" lang="'+v+'">'+languages[v]+'</a>' ;
	} ) ;
	
	$('#interface_languages').html ( h ) ;
	$('#interface_languages a.dropdown-item').click ( function () {
		var o = $(this) ;
		var l = o.attr('lang') ;
		tt.setLanguage ( l ) ;
		$('#interface_languages').parent().find('button[aria-expanded=true]').click() ;
		updateHash() ;
		return false ;
	} ) ;
}

function showToolList () {
	var h = '' ;
	h += "<ol>" ;
	$.each ( tools , function ( k , v ) {
		h += "<li>" ;
		h += "<a href='#' tool='"+k+"' class='tool_link'>" + v.label + "</a>" ;
		h += " <small>[<a href='#' tool='"+k+"' class='tool_rc_link' tt='recent_changes'></a>]</small>" ;
		h += "</li>" ;
	} ) ;
	h += "</ol>" ;
	
	$('#tool_list').html ( h ) ;
	tt.updateInterface ( $('#tool_list') ) ;
	$('#tool_list a.tool_link').click ( function () {
		var tool = $(this).attr ( 'tool' ) ;
		$('#output').html('') ;
		showTool ( tool ) ;
		return false ;
	} ) ;
	$('#tool_list a.tool_rc_link').click ( function () {
		var tool = $(this).attr ( 'tool' ) ;
		showRC ( tool ) ;
		return false ;
	} ) ;
}

function showRC ( tool , lang , offset ) {
	mode = 'rc' ;
	if ( typeof offset == 'undefined' ) offset = 0 ;
	var o = { mode:'rc' , limit:100 , offset:offset } ;
	if ( typeof tool != 'undefined' ) {
		current_tool = tool ;
		o.tool = tool ;
	}
	if ( typeof lang == 'undefined' || lang == 'all' ) {
		lang = 'all' ;
	} else {
		o.lang = lang.replace(/,.*$/,'') ; // First language only
	}
	console.log ( o ) ;
	current_languages = [ lang ] ;
	updateHash() ;
	$.get ( 'api.php' , o , function ( d ) {
		var h = '' ;
		h += "<h2 tt='recent_changes'></h2>" ;
		h += "<p>" ;
		if ( typeof tool != 'undefined' ) {
			h += "<b>" + tools[tool].label + "</b> | " ;
			h += "<a href='#' tt='rc_all_tools'></a>" ;
		} else {
			h += "<span tt='all_tools'></span>" ;
		}
		h += " | " ;
		
		h += "<select id='lang'>" ;
		h += "<option value='all'" + (lang=='all'?'selected':'') + " tt='all_languages'></option>" ;
		$.each ( languages , function ( k , v ) {
			h += "<option value='"+k+"'" + (lang==k?'selected':'') + ">" + v + "</option>" ;
		} ) ;
		h += "</select>" ;
		
		h += "</p>" ;
		h += "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr>" ;
		if ( typeof tool == 'undefined' ) h += "<th tt='tool'></th>" ;
		h += "<th tt='language'></th><th tt='user'></th><th tt='timestamp'></th><th tt='key'></th><th tt='status'></th><th tt='translation'></th><th tt='action'><th></tr></thead>" ;
		h += "<tbody>" ;
		$.each ( d.data , function ( k , v ) {
			h += "<tr>" ;
			if ( typeof tool == 'undefined' ) h += "<td nowrap>" + tools[v.tool_id].label + "</td>" ;
			h += "<td>" + v.language + "</td>" ;
			h += "<td nowrap><a href='https://meta.wikimedia.org/wiki/Special:CentralAuth/"+encodeURIComponent(v.user)+"' target='_blank'>" + v.user + "</a></td>" ;
			h += "<td>" + v.timestamp + "</td>" ;
			h += "<td>" + v.key + "</td>" ;
			h += "<td><span tt='" + (v.current?'top_edit':'') + "'></span></td>" ;
			h += "<td>" + escapeHTML ( JSON.parse(v.json) ) + "</td>" ;
//			h += "<td nowrap><a href='#' translation_id='" + v.id + "' tt='undo'></a></td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		
		$('#output').html ( h ) ;
		tt.updateInterface ( $('#output') ) ;
		$('a[tt="rc_all_tools"]').click ( function () {
			showRC ( undefined , lang , offset ) ;
			return false ;
		} ) ;
		$('#lang').change ( function () {
			var nl = $('#lang').val() ;
			showRC ( tool , nl , offset ) ;
		} ) ;
		$('a[tt="undo"]').click ( function () {
			var translation_id = $(this).attr('translation_id') ;
			$.get ( 'api.php' , {
				mode:'undo',
				translation_id:translation_id
			} , function ( d ) {
				console.log ( d ) ;
			} , 'json' ) ;
			return false ;
		} ) ;
	} , 'json' ) ;
}

$(document).ready ( function () {
	url_params = getHashVars () ;
	if ( typeof url_params.languages != 'undefined' ) current_languages = url_params.languages.split(',') ;
	if ( url_params.just_missing == 1 ) just_missing = true ;
	if ( typeof url_params.mode != 'undefined' ) mode = url_params.mode ;
	
	var running = 5 ;
	function fin() {
		running-- ;
		if ( running > 0 ) return ;
		
		showTools() ;
		showInterfaceLanguages(tooltrans_languages) ;
		if ( typeof url_params.tool != 'undefined' && typeof tools[url_params.tool] != 'undefined' ) {
			$('#output').html('') ;
			if ( mode == 'rc' ) showRC ( url_params.tool , url_params.languages ) ;
			else showTool ( url_params.tool ) ;
		} else {
			showToolList() ;
		}
		$('body').show() ;
		updateHash() ;
	}
	
	// Interface languages

	$.get ( 'api.php' , {
		mode:'get_tool_info',
		id:1 // Hardcoded: tooltranslation
	} , function ( d ) {
		tooltrans_languages = d.data.languages ;
		fin() ;
	} , 'json' ) ;
	
	// Load auth
	$.get ( 'api.php' , {
		mode:'get_rights'
	} , function ( d ) {
		auth = { result:d.result , logged_in:false } ;
		if ( typeof (((d.result||{}).query||{}).userinfo||{}).id != 'undefined' ) {
				auth.logged_in = true ;
		}
		if ( auth.logged_in ) {
			$('#auth_note').remove() ;
			$('#username').text(getUserName()) ;
			$('#new_tool').show() ;
			$('#logout').show() ;
		} else {
			$('#auth_note').show() ;
		}
		fin() ;
	} , 'json' ) ;

	// Load language list
	$.get ( 'data/languages.json' , function ( d0 ) {
		languages = d0 ;
		fin() ;
	} , 'json' ) ;

	// Load tool list
	$.get ( 'api.php?mode=get_tools' , function ( d ) {
		$.each ( d.data , function ( k , v ) {
			tools[v.id] = v ;
		} ) ;
		fin() ;
	} , 'json' ) ;

	// Load translation for this tool
	tt = new ToolTranslation ( {
		tool: 'tooltranslate' ,
		force_fresh:true ,
		debug:true,
		highlight_missing : true ,
		callback:function () {
			fin() ;
		}
	} ) ;
	
	$('a[tt="recent_changes"]').click ( function () {
		showRC() ;
		return false ;
	} ) ;
	
	$('#logout').click ( function () {
		$.get ( 'api.php' , { mode:'logout' } , function ( d ) {
			auth.logged_in = false ;
			location.reload();
		} ) ;
	} ) ;
	
	$('#new_tool').click ( function () {
		var h = "" ;
		h += "<h1 tt='new_tool_heading'></h1>" ;
		h += "<form class='form'>" ;
		h += "<table class='table'><tbody>" ;
		h += "<tr><th nowrap tt='nt_name'></th><td style='width:30%'><input class='form-control' type='text' id='nt_name' /></td><td style='width:100%'><span tt='nt_name_hint'></span></td></tr>" ;
		h += "<tr><th nowrap tt='nt_label'></th><td style='width:30%'><input class='form-control' type='text' id='nt_label' /></td><td style='width:100%'><span tt='nt_label_hint'></span></td></tr>" ;
		h += "<tr><th nowrap tt='nt_url'></th><td style='width:30%'><input class='form-control' type='text' id='nt_url' /></td><td style='width:100%'><span tt='nt_url_hint'></span></td></tr>" ;
		h += "<tr><th></th><td><button id='do_new_tool' class='btn btn-primary' tt='button_ok'></button></td><td style='width:100%'></td></tr>" ;
		h += "</tbody></table>" ;
		h += "</form>" ;
		$('#output').html ( h ) ;
		tt.updateInterface ( $('#output') ) ;
		$('#output form').submit ( function () {
			var name = $('#nt_name').val() ;
			var label = $('#nt_label').val() ;
			var url = $('#nt_url').val() ;
			name = name.replace ( /[^a-z0-9_-]+/g , '' ) ;
			if ( name == '' || label == '' ) {
				alert ( tt.t('tool_entry_warning') ) ;
				return ;
			}
			$.get ( 'api.php' , {
				mode:'add_tool',
				name:name,
				label:label,
				url:url
			} , function ( d ) {
				current_tool = d.data.tool ;
				updateHash() ;
				location.reload();
			} , 'json' ) ;
			return false ;
		} ) ;
	} ) ;
} ) ;